var numberOfSquares = 10;
var stack = [];
var visitCount = 0;
var currentSquare;
var squares = [];

function getSquare(row, column)
{
  if(column > numberOfSquares - 1 || column <= -1 || row > numberOfSquares - 1 || row <= -1)
    return undefined;
  
  return squares[row * numberOfSquares + column];
}

function setup()
{
  createCanvas(700, 700);
  for(var i = 0; i < numberOfSquares; i++)
    for(var j = 0; j < numberOfSquares; j++)
      squares.push(new Square(i, j));

  getSquare(0, 0).r = 255;
  getSquare(0, 0).visit();
}

function shuffl(a)
{
    var j, x, i;
    for (i = a.length; i; i--)
    {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}

function draw()
{
  background(51);
  stroke(255);
  for(var index = 0; index < squares.length; index++)
    squares[index].draw();
  if(visitCount != numberOfSquares * numberOfSquares)
    stack[stack.length - 1].moveToNext();
  else
  {
    stack[stack.length - 1].r = 0;
    stack[stack.length - 1].g = 255;
    stack[stack.length - 1].b = 0;
  }
}

function mousePressed()
{
  blocked = false;
}