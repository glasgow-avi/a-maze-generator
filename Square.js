function Square(row, column)
{
  this.r = 51;
  this.g = 200;
  this.b = 51;
  
  this.width = width / numberOfSquares;
  
  this.row = row;
  this.column = column;
  this.colour = random(255);
  
  this.visited = false;
  
  this.walls = [true, true, true, true];
  //top right bottom left
 
  this.visit = function()
  {
    visitCount++;
    stack.push(this);
    this.visited = true;
  }
  
  this.checkLegality = function(index, array)
  {
    if(array[index] && this.walls[index] && !array[index].visited)
    {
      array[index].visit();
      array[index].walls[(index + 2) % 4] = false;
      this.walls[index] = false;
      return true;
    }
    
    return false;
  }
  
  this.moveToNext = function()
  {
    var neighbours = [getSquare(this.row - 1, this.column), getSquare(this.row, this.column + 1), getSquare(this.row + 1, this.column), getSquare(this.row, this.column - 1)];
    var sequence = [0, 1, 2, 3];
    shuffl(sequence);
    var flag = true;
    for(var i = 0; i < sequence.length; i++)
      if(this.checkLegality(sequence[i], neighbours))
      {
        flag = false; 
        break;
      }

    if(flag)
      stack.pop();
  }
  
  this.draw = function()
  {
    push();
    if(this.visited)
      fill(this.r, this.g, this.b, 255);
    else
      noFill();
    noStroke();
    rect(this.column * this.width, this.row * this.width);
    pop();
    if(this.walls[0])
      line(this.column * this.width, this.row * this.width, (this.column + 1) * this.width, this.row * this.width);
    if(this.walls[1])
      line((this.column + 1) * this.width, this.row * this.width, (this.column + 1) * this.width, (this.row + 1) * this.width);
    if(this.walls[2])
      line((this.column + 1) * this.width, (this.row + 1) * this.width, this.column * this.width, (this.row + 1) * this.width);
    if(this.walls[3])
      line(this.column * this.width, this.row * this.width, this.column * this.width, (this.row + 1) * this.width);
  }
}